import re
import os
import sys
import json
import pystache
import pytumblr
from lxml import html

class GenerateFlatPages(object):

    def __init__(self, template_name):
        self.html_template = None
        with open('template.mustache', 'r') as f:
            html_template = f.read()

    def caption_format_one(self, _str):
        """
        u'<p>
            <img height="200"
                 width="200"
                 src="http://a.imageshack.us/img507/7987/crazysexycooltlccdcover.jpg"
                 align="left"/>
                 Creep by TLC off the Album CrazySexyCool
         </p>'
        """
        for elem in html.fragments_fromstring(_str):
            meta = elem.text_content()
            if not meta:
                continue
            pattern = re.compile(r'(?P<joint>.+) by (?P<mc>.+) off the (album|12|self titled album) (?P<album>.+)')
            try:
                formatted_meta = meta.replace(u'\xa0', u' ').encode('ascii', 'ignore').lower()
                meta_data = pattern.search(formatted_meta).groupdict()
            except AttributeError:
                import ipdb; ipdb.set_trace()
        return meta_data

    def caption_format_two(self, _str):
        elems = html.fragments_fromstring(_str)
        meta = [i.text_content() for i in elems][-1]
        pattern = re.compile(r'(?P<joint>.+) by (?P<mc>.+) off the (album|12|self titled album) (?P<album>.+)')
        try:
            formatted_meta = meta.replace(u'\xa0', u' ').encode('ascii', 'ignore').lower()
            meta_data = pattern.search(formatted_meta).groupdict()
        except AttributeError:
            print "%s" % _str
        return meta_data

    def caption_format_three(self, _str):
        elems = html.fragments_fromstring(_str)
        meta = elems[0].text_content()
        pattern = re.compile(r'(?P<joint>.+) by (?P<mc>.+) off the (album|12|self titled album) (?P<album>.+)')
        formatted_meta = meta.replace(u'\xa0', u' ').encode('ascii', 'ignore').lower()
        meta_data = pattern.search(formatted_meta).groupdict()
        return meta_data

    def parse_caption(self, raw_string):
        meta_data = {}
        try:
            if raw_string.startswith('<p><img'):
                meta_data = self.caption_format_one(raw_string)
            elif raw_string.startswith('<p><a'):
                meta_data = self.caption_format_two(raw_string)
            elif raw_string.startswith('<p><span>'):
                meta_data = self.caption_format_three(raw_string)
            else:
                import ipdb; ipdb.set_trace()
            #k, v = part.split(':')
            #meta_data.update({k.lower():v.rstrip().lower()})
        except ValueError:
            import ipdb; ipdb.set_trace
            #cap = html.fragment_fromstring(raw_string)

        if not meta_data:
            import ipdb; ipdb.set_trace()

        return meta_data

    def gen_page(self, tipe, data):
        func_name = 'gen_page_{}'.format(tipe)
        func = getattr(self, func_name)
        func(data)

    def gen_page_text(self, data):
        pass

    def gen_page_video(self, data):
        meta_data = self.parse_caption(data['caption'])
        print meta_data
        return meta_data

    def gen_page_audio(self, data):
        pass

    def gen_page_photo(self, data):
        pass

    def gen_page_answer(self, data):
        pass

def get_posts(client, url, **kwargs):
    # Authenticate via OAuth
    offset=0
    posts_len = 0
    while True:
        posts = client.posts(url, offset=offset, **kwargs)
        if not posts:
            return
        posts_len = len(posts['posts'])
        yield posts
        offset += posts_len
        print "Len: %s" % posts_len
        print "Offset: %s" % offset
        

if __name__ == '__main__':
    """
    pub_key = 'thTfZIRoyxQdykGLP9lWk4qRgf9ib6nQBvH4nicYloPk1OIF42'
    secret_key = '4sj8zayAxqnpjqpas0NszpIQ9xAxYTS9t4H9fCRZioPlvCFIMS'
    token = 'XqTomyQxpOFT0ajw0KW3OCtxkXGxeP7wlURNTyzj0DfM4gWirp'
    token_secret = 'Wd9fbQ933S9I27tZKtWsFiwRZUNawHndlqB9M9YIuKpXzeD1Ng'
    cred = [pub_key, secret_key, token, token_secret]
    client = pytumblr.TumblrRestClient(*cred)
    number = 0
    for post in get_posts(client, 'tommyjoints.tumblr.com', **dict(reblog_info=True, limit=50, notes_info=True, filter='html')):
        fname = 'post_request_{}'.format(number)
        with open(fname, 'w') as f:
            f.write(json.dumps(post, indent=4))
        print "Write {} complete".format(fname)
        number +=1

    aggregate_json_file = {}

    for i in range(11):
        fname = 'post_request_{}'.format(i)
        with open(fname, 'r') as f:
            aggregate_json_file.update(json.loads(f.read()))

    with open('concat_data.json', 'w') as f:
        f.write(json.dumps(aggregate_json_file, indent=4))
    """

    with open('concat_data.json', 'r') as f:
        post_request = json.loads(f.read())
        
    g = GenerateFlatPages('template.mustache')

    print len(post_request['posts'])
    for post in post_request['posts']:
        try:
            g.gen_page(post['type'], post)
        except Exception, ex:
            import ipdb; ipdb.set_trace()
            print ex

